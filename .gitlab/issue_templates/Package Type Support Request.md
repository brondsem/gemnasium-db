### Package Type

<!-- [Required] What is the name of the package type? Provide some background/context to illustrate why supporting the requrested package type is important. -->

### Directory structure

<!-- [Required] How should advisories for the new package type package be organized on the filesystem level?  -->

### Schema changes

<!-- [Required] If no schema changes are required, provide a short explanation as to why this is the case.
In case schema changes are required, open a separate `Schema Change Request` and link it here. -->

### Advisory example

<!-- [Required] Provide at least one concrete example that shows a complete sample advisory. -->

### What is the type of buyer?

<!-- [Optional] Which leads to: in which enterprise tier should this feature go? See https://about.gitlab.com/handbook/product/pricing/#four-tiers -->

### Intended users

<!-- [Optional] Who will use this feature? If known, include any of the following: types of users (e.g. Developer), personas, or specific company roles (e.g. Release Manager). It's okay to write "Unknown" and fill this field in later.

* [Rachel (Release Manager)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#rachel-release-manager)
* [Parker (Product Manager)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#parker-product-manager)
* [Delaney (Development Team Lead)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#delaney-development-team-lead)
* [Sasha (Software Developer)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer)
* [Presley (Product Designer)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#presley-product-designer)
* [Devon (DevOps Engineer)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#devon-devops-engineer)
* [Sidney (Systems Administrator)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sidney-systems-administrator)
* [Sam (Security Analyst)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sam-security-analyst)
* [Dana (Data Analyst)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#dana-data-analyst)
* [Simone (Software Engineer in Test)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#simone-software-engineer-in-test)
* [Allison (Application Ops)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#allison-application-ops)

Personas are described at https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/ -->

### Links / references

<!-- [Optional] Provide links to other relevant issues -->

/label ~feature ~"group::vulnerability research" ~"devops::secure" ~feature ~"Category:Vulnerability Database" 